import sys
from PyQt5.QtWidgets import (QWidget, QPushButton,
    QFrame, QApplication)
from PyQt5.QtGui import QColor
import matplotlib.pyplot as plt
import numpy as np


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        self.col = QColor(0, 0, 0)

        redb = QPushButton('curve', self)
        redb.clicked.connect(self.random_curve)
        redb.setCheckable(True)
        redb.move(10, 10)

        #redb.clicked[bool].connect(self.setColor)

        greenb = QPushButton('bar', self)
        greenb.clicked.connect(self.random_bar)
        greenb.setCheckable(True)
        greenb.move(10, 60)

        #greenb.clicked[bool].connect(self.setColor)

        blueb = QPushButton('plot', self)
        blueb.clicked.connect(self.random_plot)
        blueb.setCheckable(True)
        blueb.move(10, 110)

        #blueb.clicked[bool].connect(self.setColor)

        self.setGeometry(300, 300, 280, 170)
        self.setWindowTitle('Toggle button')
        self.show()

    def random_bar(self):
        labels = ['1', '2', '3', '4', '5']
        men_means = [np.random.randint(0, 50), np.random.randint(0, 50), np.random.randint(0, 50),
                     np.random.randint(0, 50),
                     np.random.randint(0, 50)]
        women_means = [np.random.randint(0, 50), np.random.randint(0, 50), np.random.randint(0, 50),
                       np.random.randint(0, 50),
                       np.random.randint(0, 50)]
        men_std = [1, 1, 1, 1, 1]
        women_std = [1, 1, 1, 1, 1]
        width = 0.35  # the width of the bars: can also be len(x) sequence

        fig, ax = plt.subplots()

        ax.bar(labels, men_means, width, yerr=men_std, label='sun')
        ax.bar(labels, women_means, width, yerr=women_std, bottom=men_means,
               label='rain')

        ax.set_ylabel('Scores')
        ax.set_title('Scores by weather')
        ax.legend()

        plt.show()

    def random_curve(self):
        N = np.random.randint(0, 400)
        yy = np.random.normal(0, np.pi)
        t = np.linspace(0, 2 * yy, N)
        r = 0.5 + np.cos(t)
        x, y = r * np.cos(t), r * np.sin(t)

        fig, ax = plt.subplots()
        ax.plot(x, y)
        plt.show()


    def random_plot(self):
        random_array = np.random.random_sample(20)
        random_array2 = np.random.random_sample(20)
        #self.curve.setData(random_array)

        fig, ax = plt.subplots()  # Create a figure containing a single axes.
        ax.plot(random_array, random_array2)  # Plot some data on the axes.
        plt.show()

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())